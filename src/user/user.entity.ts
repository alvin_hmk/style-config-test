import {
    Entity,
    Index,
    ManyToOne,
    PrimaryKey,
    Property,
} from '@mikro-orm/core';
import { v5 } from 'uuid';
import { UserStyleEntity } from '../style/style.entity';

// NAMESPACE_OID is a constant that is used to generate a UUID v5
// UUID RFC standard reference: https://www.rfc-editor.org/rfc/rfc4122#appendix-C
const NAMESPACE_OID = '6ba7b812-9dad-11d1-80b4-00c04fd430c8';

@Entity({ tableName: 'user' })
@Index({ properties: ['name'] })
export class UserEntity {
    @PrimaryKey({ type: 'uuid' })
    id: string;

    @Property({ nullable: false })
    name: string;

    @ManyToOne(() => UserStyleEntity, { 
        nullable: false,
        unique: false,
        onDelete: 'cascade',
    })
    styleConfig: UserStyleEntity;

    constructor(name: string) {
        this.id = v5(name, NAMESPACE_OID);
        this.name = name;
        // this.fontFamily = fontFamily;
    }
}
