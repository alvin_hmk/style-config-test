import {
    Entity,
    Index,
    PrimaryKey,
    Property,
} from '@mikro-orm/core';
import { v5 } from 'uuid';
import { FontType } from './enum/StyleType';

// NAMESPACE_OID is a constant that is used to generate a UUID v5
// UUID RFC standard reference: https://www.rfc-editor.org/rfc/rfc4122#appendix-C
const NAMESPACE_OID = '6ba7b812-9dad-11d1-80b4-00c04fd430c8';

@Entity({ tableName: 'user_style' })
@Index({ properties: ['user'] })
export class UserStyleEntity {
    @PrimaryKey({ type: 'uuid' })
    id: string;

    @Property({ nullable: false })
    user: string;

    @Property({ nullable: false })
    fontFamily: FontType;

    constructor(fontFamily: FontType) {
        this.id = v5(fontFamily, NAMESPACE_OID);
        this.fontFamily = fontFamily;
    }
}


@Entity({ tableName: 'tenant_style' })
@Index({ properties: ['tenant'] })
export class TenantStyleEntity {
    @PrimaryKey({ type: 'uuid' })
    id: string;

    @Property({ nullable: false })
    tenant: string;

    @Property({ nullable: false })
    fontFamily: FontType;

    constructor(fontFamily: FontType) {
        this.id = v5(fontFamily, NAMESPACE_OID);
        this.fontFamily = fontFamily;
    }
}


