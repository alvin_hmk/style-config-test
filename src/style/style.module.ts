import { Module } from "@nestjs/common";

import { StyleResolver } from "./style.resolver";
import { StyleService } from "./style.service";

@Module({
    providers: [StyleService, StyleResolver],
})
export class StyleModule { }