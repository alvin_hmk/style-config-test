import { Field, ObjectType } from '@nestjs/graphql';

import {FontType} from './enum';

@ObjectType()
export class Style {
    @Field()
    hororBackgroundColor: string;

    @Field()
    invertHororBackgroundColor: string;

    @Field()
    disgustPColor: string;

    @Field()
    invertDisgustPColor: string;

    @Field()
    crazyColor: string;

    @Field()
    idiotFontSize: string;

    @Field()
    madFontFamily: string;

    @Field()
    fooBackgroundColor: string;

    @Field()
    crazyHoverColor: string;
}

@ObjectType()
export class FontStyle {
    @Field()
    fontFamily: FontType;
}
