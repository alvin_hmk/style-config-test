import { Injectable } from '@nestjs/common';

import { FontType } from './enum';
import { Style, FontStyle } from './style.model';

@Injectable()
export class StyleService {
  getStyle(): Style {
    const theme = {
      hororBackgroundColor: 'lightyellow',
      invertHororBackgroundColor: 'black',

      disgustPColor: 'black',
      invertDisgustPColor: 'lightorange',

      crazyColor: 'red',
      idiotFontSize: '15px',
      madFontFamily: "'Snell Roundhand', 'cursive'",
      fooBackgroundColor: 'rgba(0, 0, 0, 0)',
      crazyHoverColor: 'blue',
    }

    console.log('getStyle() called');

    return theme;
  }

  getFontStyle(): FontStyle {
    return {
      fontFamily: FontType.Snell_Roundhand,
    }
  }

  setFontStyle(setFontFamily: FontType): FontStyle {
    return {
      fontFamily: setFontFamily,
    }
  }
}
