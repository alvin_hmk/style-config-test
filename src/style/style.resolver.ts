import { Mutation, Query, Resolver } from "@nestjs/graphql";

import { Style, FontStyle } from "./style.model";
import { StyleService } from "./style.service";
import { FontType } from "./enum";

@Resolver()
export class StyleResolver {
    constructor(
        private styleService: StyleService,
    ) {}

    @Query(() => Style)
    getStyle(): Style {
        return this.styleService.getStyle();
    }

    @Query(() => FontStyle)
    getFontStyle(): FontStyle {
        return this.styleService.getFontStyle();
    };

    @Mutation(() => FontStyle)
    setFontStyle(): FontStyle {
        this.styleService.setFontStyle(FontType.Montserrat);
        return this.styleService.getFontStyle();
    };
}