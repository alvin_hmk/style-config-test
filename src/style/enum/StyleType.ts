import {registerEnumType} from '@nestjs/graphql';

export enum FontType {
    Snell_Roundhand = "'Snell Roundhand', 'cursive'",
    Montserrat = "'Montserrat', 'sans-serif'",
}

registerEnumType(FontType, {
    name: 'FontType',
});