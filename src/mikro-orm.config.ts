import { Options } from '@mikro-orm/core';

const config: Options = {
    entities: ['dist/**/*.entity.js'],
    entitiesTs: ['src/**/*.entity.ts'],
    dbName: 'postgres',
    type: 'postgresql',
    host: 'localhost',
    port: 5434,
    user: 'postgres',
    password: 'P@ssw0rd',
    seeder: {
        pathTs: 'src/seeder',
        defaultSeeder: 'FontStyleSeeder',
        emit: 'ts',
        fileName: (className: string) => className, // seeder file naming convention
    }
};

export default config;