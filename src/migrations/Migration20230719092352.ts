import { Migration } from '@mikro-orm/migrations';

export class Migration20230719092352 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "style" ("id" uuid not null, "font_family" varchar(255) not null, constraint "style_pkey" primary key ("id"));');
    this.addSql('create index "style_font_family_index" on "style" ("font_family");');
  }

}
