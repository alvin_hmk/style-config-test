import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { MikroOrmModule } from '@mikro-orm/nestjs';

import { StyleModule } from './style/style.module';
import { SubscriptionModule } from './subscription/subscription.module';

@Module({
  imports: [
    StyleModule,
    SubscriptionModule,
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: 'schema.gql',
      sortSchema: true,
      subscriptions: {
        'graphql-ws': {
          path: '/subscriptions',
        },
      },
    }),
    MikroOrmModule.forRoot()
  ]
})
export class AppModule { }
