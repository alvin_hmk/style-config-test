import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): object {
    const theme: object = {
      hororBackgroundColor: 'white',
      invertHororBackgroundColor: 'black',
  
      disgustPColor: 'black',
      invertDisgustPColor: 'white',
  
      crazyColor: 'red',
      idiotFontSize: '15px',
      damnBackgroundColor: 'rgba(0, 0, 0, 0)',
      crazyHoverColor: 'blue',
    }
  
    return theme;
  }
}
