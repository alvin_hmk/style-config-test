import { Args, Mutation, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'graphql-subscriptions';

import { Message } from './subscription.model';

const pubSub = new PubSub();

@Resolver('Subscription')
export class SubscriptionResolver {
  @Subscription(returns => Message, {
    name: 'newMessage',
  })
  newMessage() {
    return pubSub.asyncIterator('newMessage');
  }

  @Mutation(returns => Number)
  async addMessage(
    @Args('number', { type: () => Number }) number: number,
  ) {
    const ran_number = Math.floor(Math.random() * 10);
    const set_number = number ? number : ran_number;
    pubSub.publish('newMessage', { newMessage: { set_number } });
    return set_number;
  }
}