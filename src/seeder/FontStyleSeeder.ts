import type {EntityManager} from '@mikro-orm/core';
import {Seeder} from '@mikro-orm/seeder';
import {FontType} from '../style/enum/';
import {UserStyleEntity} from '../style/style.entity';

export class FontStyleSeeder extends Seeder {
  async run(em: EntityManager): Promise<void> {
    // Improve performance by using Set.has() to replace Array.includes.
    // Ref: https://www.tech-hour.com/javascript-performance-and-optimization
    const existingTypes = new Set(
      (await em.find(UserStyleEntity, {})).map(entry => entry.fontFamily)
    );
    const newTypesToInsert = Object.values(FontType).filter(
      type => !existingTypes.has(type)
    );

    if (newTypesToInsert.length === 0) {
      console.warn(
        'All widget types has already been inserted. No new records would be added.'
      );
      return;
    }

    console.log(
      `The following widget types would be added: ${newTypesToInsert}`
    );
    const fontStyleTypes = newTypesToInsert.map((typeName: FontType) => {
      return new UserStyleEntity(typeName);
    });
    await em.persistAndFlush(fontStyleTypes);
  }
}
